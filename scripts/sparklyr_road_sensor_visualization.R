### time series for one specific road sensor

# get data for one sensor, and aggregate it per hour
df <- y %>% 
  filter(measurementSiteReference == "RWS01_MONICA_00D04F00045F80200005",
         specificVehicleCharacteristics == 0) %>% 
  mutate(hour = floor(minute / 60)) %>%        # minute %/% 60 doesn't work
  group_by(daynr, hour) %>% 
  summarise(value = mean(value)) %>% 
  collect()
  
## add datetime column
df2 <- df %>% 
  mutate(date = as.Date("2018-01-01", tz = "CET") + daynr - 1) %>% 
  mutate(datetime = as.POSIXlt(date) + (hour + .5) * 60 * 60)

## static time series chart
library(ggplot2)
library(scales)

ggplot(df2, aes(x = datetime, y = value)) +
  geom_line()

## interactive time series chart
library(dygraphs)
library(xts) 

ts <- xts(x = df2$value, order.by = df2$datetime)

dygraph(ts) %>%
  dyOptions( drawPoints = TRUE, pointSize = 2)


############################################
### map
############################################

library(sf)
library(tmap)


## get data for one day, aggregate it per sensor, per hour
df3 <- y %>% filter(specificVehicleCharacteristics == 0,
                   daynr == 335) %>% 
  mutate(hour = floor(minute / 60)) %>% 
  group_by(measurementSiteReference, hour) %>% 
  summarize(value = mean(value)) %>% 
  collect()

## get meta data including the coordinates and create a spatial (sf) object
meta <- read_csv("data/sensors_a79.csv")
meta <- st_as_sf(meta, coords = c("long", "lat"), crs = 4326)

## view the locations of the sensors
tmap_mode("view")
qtm(meta)

## color by direction
tm_shape(meta) +
  tm_dots(col = "direction")

## format the counts data in wide format and join it to meta
df4 <- df3 %>% 
  mutate(hour = paste0("t", hour)) %>% 
  pivot_wider(names_from = hour, values_from = value)




meta <- meta %>% 
  left_join(df4, by = c("siteRef" = "measurementSiteReference")) %>% 
  filter(!is.na(t0))

# show the counts at 12:00
tm_shape(meta) +
  tm_dots(col = "t12", size = 0.1)

# show the countrs at 12:00 per direction
tm_shape(meta %>% filter(direction == "L")) +
  tm_dots(col = "t12", size = 0.1, palette = "YlGnBu", breaks = seq(0, 15, by = 3), title = "<-") +
tm_shape(meta %>% filter(direction == "R")) +
  tm_dots(col = "t12", size = 0.1, palette = "YlOrRd", breaks = seq(0, 15, by = 3), title = "->")

tm_shape(meta) +
  tm_dots(col = paste0("t", 0:23), size = 0.1, palette = "viridis", breaks = seq(0, 18, by = 3))
